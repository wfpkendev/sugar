<?php


require_once('service/v4/SugarWebServiceImplv4.php');
require_once('custom/zuckerreports_service/v4/SugarWebServiceImpl_v4_custom.php');

class SugarWebService_v4_custom extends SugarWebService
{
	protected $implementationClass = 'SugarWebServiceImpl_v4_custom';
}

?>