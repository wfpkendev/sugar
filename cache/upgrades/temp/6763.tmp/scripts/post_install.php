<?php


function post_install( ) {

	if (is_file('modules/zr2_ReportContainer/zr2_ReportContainer.php')) {
		require_once('modules/zr2_ReportContainer/zr2_ReportContainer.php');
		
		$seed = new zr2_ReportContainer();
		$seed->name = "Archive";
		$seed->save();
	}
	if (is_file('modules/zr2_ReportParameter/zr2_ReportParameter.php')) {
		require_once('modules/zr2_ReportParameter/zr2_ReportParameter.php');

		$seed = new zr2_ReportParameter();
		$seed->name = "Account";
		$seed->friendly_name = "Account";
		$seed->default_name = "ACCOUNT_ID";
		$seed->default_value = "";
		$seed->description = "";
		$seed->range_name = "SQL";
		$seed->range_options = 'select id, name from accounts where deleted = 0 order by name';
		$seed->save();
		
		$seed = new zr2_ReportParameter();
		$seed->name = "Contact";
		$seed->friendly_name = "Contact";
		$seed->default_name = "CONTACT_ID";
		$seed->default_value = "";
		$seed->description = "";
		$seed->range_name = "SQL";
		$seed->range_options = 'select id, concat(last_name, concat(" ", first_name)) as name from contacts where deleted = 0 order by last_name';
		$seed->save();

		$seed = new zr2_ReportParameter();
		$seed->name = "Meeting";
		$seed->friendly_name = "Meeting";
		$seed->default_name = "MEETING_ID";
		$seed->default_value = "";
		$seed->description = "";
		$seed->range_name = "SQL";
		$seed->range_options = 'select id, concat(name, " (", date_start, ")") from meetings where deleted = 0 order by name, date_start';
		$seed->save();

		$seed = new zr2_ReportParameter();
		$seed->name = "Project";
		$seed->friendly_name = "Project";
		$seed->default_name = "PROJECT_ID";
		$seed->default_value = "";
		$seed->description = "";
		$seed->range_name = "SQL";
		$seed->range_options = 'select id, name from project where deleted = 0 order by name';
		$seed->save();
		
		$seed = new zr2_ReportParameter();
		$seed->name = "Current User";
		$seed->friendly_name = "Current User";
		$seed->default_name = "CURRENT_USER";
		$seed->default_value = "";
		$seed->description = "";
		$seed->range_name = "CURRENT_USER";
		$seed->save();

		$seed = new zr2_ReportParameter();
		$seed->name = "My Script";
		$seed->friendly_name = "My Script";
		$seed->default_name = "MY_SCRIPT";
		$seed->default_value = "";
		$seed->description = "";
		$seed->range_name = "SCRIPT";
		$seed->range_options = "return '%';";
		$seed->save();
		
	}
}

?>
