{
	"properties": [

	{

		"title":""
,
		"subtitle":"Opportunity size in $1K"
,
		"type":"horizontal group by chart"
,
		"legend":"on"
,
		"labels":"value"
,
		"thousands":""

	}

	],

	"label": [

		"Prospecting"
,
		"Qualification"
,
		"Needs Analysis"
,
		"Value Proposition"
,
		"Id. Decision Makers"
,
		"Perception Analysis"
,
		"Proposal/Price Quote"
,
		"Negotiation/Review"
,
		"Closed Won"
,
		"Closed Lost"

	],

	"color": [

		"#8c2b2b"
,
		"#468c2b"
,
		"#2b5d8c"
,
		"#cd5200"
,
		"#e6bf00"
,
		"#7f3acd"
,
		"#00a9b8"
,
		"#572323"
,
		"#004d00"
,
		"#000087"
,
		"#e48d30"
,
		"#9fba09"
,
		"#560066"
,
		"#009f92"
,
		"#b36262"
,
		"#38795c"
,
		"#3D3D99"
,
		"#99623d"
,
		"#998a3d"
,
		"#994e78"
,
		"#3d6899"
,
		"#CC0000"
,
		"#00CC00"
,
		"#0000CC"
,
		"#cc5200"
,
		"#ccaa00"
,
		"#6600cc"
,
		"#005fcc"

	],

	"values": [

	{

		"label": "",

		"gvalue": "120",

		"gvaluelabel": "$120.00K",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			35
,
			75
,
			10


		],

		"valuelabels": [
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$35.00K"
,
			"$75.00K"
,
			"$10.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Cold Call",

		"gvalue": "210",

		"gvaluelabel": "$210.00K",

		"values": [
			85
,
			75
,
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			50


		],

		"valuelabels": [
			"$85.00K"
,
			"$75.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$50.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Existing Customer",

		"gvalue": "160",

		"gvaluelabel": "$160.00K",

		"values": [
			25
,
			0
,
			0
,
			10
,
			0
,
			0
,
			125
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$10.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$125.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Self Generated",

		"gvalue": "100",

		"gvaluelabel": "$100.00K",

		"values": [
			50
,
			25
,
			0
,
			0
,
			25
,
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"$50.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Employee",

		"gvalue": "60",

		"gvaluelabel": "$60.00K",

		"values": [
			25
,
			0
,
			10
,
			0
,
			0
,
			0
,
			25
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"$25.00K"
,
			"$0.00K"
,
			"$10.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Partner",

		"gvalue": "135",

		"gvaluelabel": "$135.00K",

		"values": [
			0
,
			50
,
			0
,
			0
,
			75
,
			0
,
			0
,
			0
,
			10
,
			0


		],

		"valuelabels": [
			"$0.00K"
,
			"$50.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$75.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$10.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Public Relations",

		"gvalue": "170",

		"gvaluelabel": "$170.00K",

		"values": [
			75
,
			0
,
			10
,
			0
,
			0
,
			0
,
			10
,
			0
,
			0
,
			75


		],

		"valuelabels": [
			"$75.00K"
,
			"$0.00K"
,
			"$10.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$10.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$75.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Direct Mail",

		"gvalue": "60",

		"gvaluelabel": "$60.00K",

		"values": [
			0
,
			10
,
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			50
,
			0


		],

		"valuelabels": [
			"$0.00K"
,
			"$10.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$50.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Conference",

		"gvalue": "175",

		"gvaluelabel": "$175.00K",

		"values": [
			0
,
			25
,
			0
,
			0
,
			0
,
			0
,
			75
,
			0
,
			0
,
			75


		],

		"valuelabels": [
			"$0.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$75.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$75.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Trade Show",

		"gvalue": "135",

		"gvaluelabel": "$135.00K",

		"values": [
			50
,
			0
,
			0
,
			0
,
			0
,
			10
,
			75
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"$50.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$10.00K"
,
			"$75.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Web Site",

		"gvalue": "210",

		"gvaluelabel": "$210.00K",

		"values": [
			50
,
			0
,
			25
,
			50
,
			0
,
			75
,
			0
,
			0
,
			0
,
			10


		],

		"valuelabels": [
			"$50.00K"
,
			"$0.00K"
,
			"$25.00K"
,
			"$50.00K"
,
			"$0.00K"
,
			"$75.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$10.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Word of mouth",

		"gvalue": "75",

		"gvaluelabel": "$75.00K",

		"values": [
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			0
,
			75
,
			0
,
			0


		],

		"valuelabels": [
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$75.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Email",

		"gvalue": "100",

		"gvaluelabel": "$100.00K",

		"values": [
			50
,
			25
,
			0
,
			0
,
			0
,
			0
,
			0
,
			25
,
			0
,
			0


		],

		"valuelabels": [
			"$50.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Campaign",

		"gvalue": "300",

		"gvaluelabel": "$300.00K",

		"values": [
			0
,
			0
,
			50
,
			0
,
			100
,
			0
,
			0
,
			25
,
			125
,
			0


		],

		"valuelabels": [
			"$0.00K"
,
			"$0.00K"
,
			"$50.00K"
,
			"$0.00K"
,
			"$100.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$25.00K"
,
			"$125.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign%26sales_stage%3DClosed%2BLost"


		]

	}
,
	{

		"label": "Other",

		"gvalue": "75",

		"gvaluelabel": "$75.00K",

		"values": [
			0
,
			25
,
			0
,
			50
,
			0
,
			0
,
			0
,
			0
,
			0
,
			0


		],

		"valuelabels": [
			"$0.00K"
,
			"$25.00K"
,
			"$0.00K"
,
			"$50.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"
,
			"$0.00K"


		],

		"links": [
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DProspecting"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DQualification"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DNeeds%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DValue%2BProposition"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DId.%2BDecision%2BMakers"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DPerception%2BAnalysis"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DProposal%252FPrice%2BQuote"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DNegotiation%252FReview"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DClosed%2BWon"
,
			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther%26sales_stage%3DClosed%2BLost"


		]

	}

	]

}