{
	"properties": [

	{

		"title":""
,
		"subtitle":"Opportunity size in $1K"
,
		"type":"pie chart"
,
		"legend":"on"
,
		"labels":"value"
,
		"thousands":""

	}

	],

	"label": [

		""
,
		"Cold Call"
,
		"Existing Customer"
,
		"Self Generated"
,
		"Employee"
,
		"Partner"
,
		"Public Relations"
,
		"Direct Mail"
,
		"Conference"
,
		"Trade Show"
,
		"Web Site"
,
		"Word of mouth"
,
		"Email"
,
		"Campaign"
,
		"Other"

	],

	"color": [

		"#8c2b2b"
,
		"#468c2b"
,
		"#2b5d8c"
,
		"#cd5200"
,
		"#e6bf00"
,
		"#7f3acd"
,
		"#00a9b8"
,
		"#572323"
,
		"#004d00"
,
		"#000087"
,
		"#e48d30"
,
		"#9fba09"
,
		"#560066"
,
		"#009f92"
,
		"#b36262"
,
		"#38795c"
,
		"#3D3D99"
,
		"#99623d"
,
		"#998a3d"
,
		"#994e78"
,
		"#3d6899"
,
		"#CC0000"
,
		"#00CC00"
,
		"#0000CC"
,
		"#cc5200"
,
		"#ccaa00"
,
		"#6600cc"
,
		"#005fcc"

	],

	"values": [

	{

		"label": [

			""

		],

		"values": [

			120

		],

		"valuelabels": [

			"$120.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3D"

		]

	}
,
	{

		"label": [

			"Cold Call"

		],

		"values": [

			210

		],

		"valuelabels": [

			"$210.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCold%2BCall"

		]

	}
,
	{

		"label": [

			"Existing Customer"

		],

		"values": [

			160

		],

		"valuelabels": [

			"$160.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DExisting%2BCustomer"

		]

	}
,
	{

		"label": [

			"Self Generated"

		],

		"values": [

			100

		],

		"valuelabels": [

			"$100.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DSelf%2BGenerated"

		]

	}
,
	{

		"label": [

			"Employee"

		],

		"values": [

			60

		],

		"valuelabels": [

			"$60.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmployee"

		]

	}
,
	{

		"label": [

			"Partner"

		],

		"values": [

			135

		],

		"valuelabels": [

			"$135.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPartner"

		]

	}
,
	{

		"label": [

			"Public Relations"

		],

		"values": [

			170

		],

		"valuelabels": [

			"$170.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DPublic%2BRelations"

		]

	}
,
	{

		"label": [

			"Direct Mail"

		],

		"values": [

			60

		],

		"valuelabels": [

			"$60.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DDirect%2BMail"

		]

	}
,
	{

		"label": [

			"Conference"

		],

		"values": [

			175

		],

		"valuelabels": [

			"$175.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DConference"

		]

	}
,
	{

		"label": [

			"Trade Show"

		],

		"values": [

			135

		],

		"valuelabels": [

			"$135.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DTrade%2BShow"

		]

	}
,
	{

		"label": [

			"Web Site"

		],

		"values": [

			210

		],

		"valuelabels": [

			"$210.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWeb%2BSite"

		]

	}
,
	{

		"label": [

			"Word of mouth"

		],

		"values": [

			75

		],

		"valuelabels": [

			"$75.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DWord%2Bof%2Bmouth"

		]

	}
,
	{

		"label": [

			"Email"

		],

		"values": [

			100

		],

		"valuelabels": [

			"$100.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DEmail"

		]

	}
,
	{

		"label": [

			"Campaign"

		],

		"values": [

			300

		],

		"valuelabels": [

			"$300.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DCampaign"

		]

	}
,
	{

		"label": [

			"Other"

		],

		"values": [

			75

		],

		"valuelabels": [

			"$75.00K"

		],

		"links": [

			"index.php%3Fmodule%3DOpportunities%26action%3Dindex%26query%3Dtrue%26searchFormTab%3Dadvanced_search%26lead_source%3DOther"

		]

	}

	
]

}