<?php
$module_name = 'field_Helpdesk';
$searchdefs [$module_name] = 
array (
  'layout' => 
  array (
    'basic_search' => 
    array (
      'household_number' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_HOUSEHOLD_NUMBER',
        'width' => '10%',
        'default' => true,
        'name' => 'household_number',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
    ),
    'advanced_search' => 
    array (
      'household_number' => 
      array (
        'type' => 'varchar',
        'label' => 'LBL_HOUSEHOLD_NUMBER',
        'width' => '10%',
        'default' => true,
        'name' => 'household_number',
      ),
      'name' => 
      array (
        'name' => 'name',
        'default' => true,
        'width' => '10%',
      ),
      'date_entered' => 
      array (
        'type' => 'datetime',
        'label' => 'LBL_DATE_ENTERED',
        'width' => '10%',
        'default' => true,
        'name' => 'date_entered',
      ),
    ),
  ),
  'templateMeta' => 
  array (
    'maxColumns' => '3',
    'maxColumnsBasic' => '4',
    'widths' => 
    array (
      'label' => '10',
      'field' => '30',
    ),
  ),
);
?>
