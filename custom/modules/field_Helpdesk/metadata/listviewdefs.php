<?php
$module_name = 'field_Helpdesk';
$listViewDefs [$module_name] = 
array (
  'HOUSEHOLD_NUMBER' => 
  array (
    'type' => 'varchar',
    'label' => 'LBL_HOUSEHOLD_NUMBER',
    'width' => '10%',
    'default' => true,
	'link' => true,
  ),
  'NAME' => 
  array (
    'width' => '32%',
    'label' => 'LBL_NAME',
    'default' => true,
    'link' => false,
  ),
  'DATE_ENTERED' => 
  array (
    'type' => 'datetime',
    'label' => 'LBL_DATE_ENTERED',
    'width' => '6%',
    'default' => true,
  ),
  'CREATED_BY_NAME' => 
  array (
    'type' => 'relate',
    'link' => true,
    'label' => 'LBL_CREATED',
    'id' => 'CREATED_BY',
    'width' => '10%',
    'default' => true,
  ),
);
?>
