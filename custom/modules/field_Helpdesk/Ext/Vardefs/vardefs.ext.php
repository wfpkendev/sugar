<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2016-02-09 14:06:07
$dictionary["field_Helpdesk"]["fields"]["field_helpdesk_cases"] = array (
  'name' => 'field_helpdesk_cases',
  'type' => 'link',
  'relationship' => 'field_helpdesk_cases',
  'source' => 'non-db',
  'module' => 'Cases',
  'bean_name' => 'Case',
  'side' => 'right',
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_CASES_TITLE',
);

?>