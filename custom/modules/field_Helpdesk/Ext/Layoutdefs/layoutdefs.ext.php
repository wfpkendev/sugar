<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2016-02-09 14:06:07
$layout_defs["field_Helpdesk"]["subpanel_setup"]['field_helpdesk_cases'] = array (
  'order' => 100,
  'module' => 'Cases',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_FIELD_HELPDESK_CASES_FROM_CASES_TITLE',
  'get_subpanel_data' => 'field_helpdesk_cases',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);


//auto-generated file DO NOT EDIT
$layout_defs['field_Helpdesk']['subpanel_setup']['field_helpdesk_cases']['override_subpanel_name'] = 'field_Helpdesk_subpanel_field_helpdesk_cases';

?>