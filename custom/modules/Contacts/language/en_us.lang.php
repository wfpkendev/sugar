<?php
// created: 2014-04-24 16:32:01
$mod_strings = array (
  'LBL_ACCOUNT_NAME' => 'Account Name:',
  'LBL_ACCOUNT_ID' => 'Account ID:',
  'LBL_ACCOUNT' => 'Account',
  'LBL_CASES' => 'Complaints',
  'LBL_LIST_ACCOUNT_NAME' => 'Account Name',
);