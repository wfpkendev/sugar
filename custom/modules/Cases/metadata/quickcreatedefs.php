<?php
$viewdefs ['Cases'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/Cases/dependent_dropdowns.js',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'DEFAULT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'default' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => 
          array (
            'name' => 'information_source_c',
            'studio' => 'visible',
            'label' => 'LBL_INFORMATION_SOURCE',
          ),
        ),
        1 => 
        array (
          0 => 'status',
          1 => 
          array (
            'name' => 'caller_gender_c',
            'studio' => 'visible',
            'label' => 'LBL_CALLER_GENDER',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'complaint_county_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPLAINT_COUNTY',
            'displayParams' => 
            array (
              'javascript' => 'onchange="initData();"',
            ),
          ),
          1 => 
          array (
            'name' => 'fdp_list_c',
            'studio' => 'visible',
            'label' => 'LBL_FDP_LIST',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'cp_c',
            'studio' => 'visible',
            'label' => 'LBL_CP',
          ),
          1 => '',
        ),
        4 => 
        array (
          0 => 'description',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'further_action_c',
            'studio' => 'visible',
            'label' => 'LBL_FURTHER_ACTION',
          ),
          1 => '',
        ),
        6 => 
        array (
          0 => 
          array (
            'name' => 'resolution',
            'comment' => 'The resolution of the case',
            'label' => 'LBL_RESOLUTION',
          ),
        ),
        7 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
      ),
    ),
  ),
);
?>
