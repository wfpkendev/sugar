<?php
$viewdefs ['Cases'] = 
array (
  'EditView' => 
  array (
    'templateMeta' => 
    array (
      'includes' => 
      array (
        0 => 
        array (
          'file' => 'custom/modules/Cases/dependent_dropdowns.js',
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL3' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_CASE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'case_number',
            'type' => 'readonly',
          ),
          1 => 
          array (
            'name' => 'date_entered',
            'comment' => 'Date record created',
            'label' => 'LBL_DATE_ENTERED',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'created_by_name',
            'label' => 'LBL_CREATED',
            'type' => 'readonly',
          ),
          1 => 'priority',
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'complaintsource_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPLAINTSOURCE',
          ),
          1 => 
          array (
            'name' => 'complainant_telephone_c',
            'label' => 'LBL_COMPLAINANT_TELEPHONE',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'information_source_c',
            'studio' => 'visible',
            'label' => 'LBL_INFORMATION_SOURCE',
          ),
          1 => 
          array (
            'name' => 'caller_gender_c',
            'studio' => 'visible',
            'label' => 'LBL_CALLER_GENDER',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'complaint_county_c',
            'studio' => 'visible',
            'label' => 'LBL_COMPLAINT_COUNTY',
            'displayParams' => 
            array (
              'javascript' => 'onchange="initData();"',
            ),
          ),
          1 => 
          array (
            'name' => 'fdp_list_c',
            'studio' => 'visible',
            'label' => 'LBL_FDP_LIST',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'household_number_c',
            'label' => 'LBL_HOUSEHOLD_NUMBER',
          ),
          1 => 
          array (
            'name' => 'cp_c',
            'studio' => 'visible',
            'label' => 'LBL_CP',
          ),
        ),
      ),
      'lbl_editview_panel3' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'bamba_chakula_number_c',
            'label' => 'LBL_BAMBA_CHAKULA_NUMBER',
          ),
          1 => 
          array (
            'name' => 'family_size_c',
            'label' => 'LBL_FAMILY_SIZE',
          ),
        ),
      ),
      'lbl_case_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'programme_c',
            'studio' => 'visible',
            'label' => 'LBL_PROGRAMME',
          ),
          1 => 
          array (
            'name' => 'case_category_c',
            'studio' => 'visible',
            'label' => 'LBL_CASE_CATEGORY',
          ),
        ),
        1 => 
        array (
          0 => 'status',
          1 => 
          array (
            'name' => 'date_closed_c',
            'label' => 'LBL_DATE_CLOSED',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'displayParams' => 
            array (
              'size' => 75,
            ),
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'nl2br' => true,
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'further_action_c',
            'studio' => 'visible',
            'label' => 'LBL_FURTHER_ACTION',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'resolution',
            'nl2br' => true,
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
        ),
      ),
    ),
  ),
);
?>
