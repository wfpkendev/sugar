<?php 
 //WARNING: The contents of this file are auto-generated


// created: 2016-02-09 14:06:07
$dictionary["Case"]["fields"]["field_helpdesk_cases"] = array (
  'name' => 'field_helpdesk_cases',
  'type' => 'link',
  'relationship' => 'field_helpdesk_cases',
  'source' => 'non-db',
  'module' => 'field_Helpdesk',
  'bean_name' => false,
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_FIELD_HELPDESK_TITLE',
  'id_name' => 'field_helpdesk_casesfield_helpdesk_ida',
);
$dictionary["Case"]["fields"]["field_helpdesk_cases_name"] = array (
  'name' => 'field_helpdesk_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_FIELD_HELPDESK_TITLE',
  'save' => true,
  'id_name' => 'field_helpdesk_casesfield_helpdesk_ida',
  'link' => 'field_helpdesk_cases',
  'table' => 'field_helpdesk',
  'module' => 'field_Helpdesk',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["field_helpdesk_casesfield_helpdesk_ida"] = array (
  'name' => 'field_helpdesk_casesfield_helpdesk_ida',
  'type' => 'link',
  'relationship' => 'field_helpdesk_cases',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_CASES_TITLE',
);


 // created: 2015-09-17 18:02:48
$dictionary['Case']['fields']['bamba_chakula_number_c']['labelValue']='Bamba Chakula number';

 

 // created: 2016-02-10 11:50:35
$dictionary['Case']['fields']['caller_gender_c']['labelValue']='Gender of Complainant';

 

 // created: 2015-09-17 17:41:06
$dictionary['Case']['fields']['case_category_c']['labelValue']='Case Category';

 

 // created: 2015-09-25 17:52:48
$dictionary['Case']['fields']['complainant_telephone_c']['labelValue']='Telephone Number';

 

 // created: 2013-05-16 15:15:34

 

 // created: 2016-02-10 11:51:50
$dictionary['Case']['fields']['complaint_county_c']['labelValue']='Region';

 

 // created: 2013-05-14 10:42:28

 

 // created: 2015-09-17 17:34:56
$dictionary['Case']['fields']['cp_c']['labelValue']='Cooperating partner (CP)';

 

 // created: 2016-12-30 05:42:59
$dictionary['Case']['fields']['date_closed_c']['labelValue']='Date Closed';

 

 // created: 2013-05-16 15:42:07

 

 // created: 2016-02-10 11:52:27
$dictionary['Case']['fields']['family_size_c']['labelValue']='Family size';

 

 // created: 2016-02-12 05:38:00
$dictionary['Case']['fields']['fdp_list_c']['labelValue']='FDP';

 

 // created: 2015-09-17 18:22:08
$dictionary['Case']['fields']['further_action_c']['labelValue']='Further action';

 

 // created: 2016-12-01 15:46:54
$dictionary['Case']['fields']['household_number_c']['labelValue']='Household number';

 

 // created: 2016-02-10 11:48:34
$dictionary['Case']['fields']['information_source_c']['labelValue']='Source of Information';

 

 // created: 2015-09-21 15:47:25
$dictionary['Case']['fields']['name']['required']=false;
$dictionary['Case']['fields']['name']['comments']='The short description of the bug';
$dictionary['Case']['fields']['name']['merge_filter']='disabled';

 

 // created: 2015-09-17 17:35:18
$dictionary['Case']['fields']['priority']['default']='P3';
$dictionary['Case']['fields']['priority']['comments']='The priority of the case';
$dictionary['Case']['fields']['priority']['merge_filter']='disabled';

 

 // created: 2015-09-17 18:04:38
$dictionary['Case']['fields']['programme_c']['labelValue']='Programme';

 

 // created: 2016-06-07 11:50:42
$dictionary['Case']['fields']['test_field_c']['labelValue']='test field';

 
?>