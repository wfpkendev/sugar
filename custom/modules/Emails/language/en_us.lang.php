<?php
// created: 2014-04-24 16:31:59
$mod_strings = array (
  'LBL_EMAILS_ACCOUNTS_REL' => 'Emails:Accounts',
  'LBL_EMAILS_CASES_REL' => 'Emails:Complaints',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'Accounts',
  'LBL_CASES_SUBPANEL_TITLE' => 'Complaints',
);