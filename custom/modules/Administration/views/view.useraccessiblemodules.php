<style>
    table td div{
        display:'inline'; float: left;
        width:15%;background-color:#fff;
        font-size: 14px;
    }
</style>
<?php
require_once("modules/MySettings/TabController.php");

class AdministrationViewUserAccessibleModules extends SugarView {

    public function AdministrationViewUserAccessibleModules() {
        parent::SugarView();
    }

    public function display() {
        require_once('custom/Licence/classes/Licenceutils.php');
        $checkCrmmobSubscription = Licenceutils::validateCRMMobSubscription();
        if (!$checkCrmmobSubscription['success']) {
            if (!empty($checkCrmmobSubscription['message'])) {
                echo '<div style="color: #F11147;text-align: center;background: #FAD7EC;padding: 10px;margin: 3% auto;width: 70%;top: 50%;left: 0;right: 0;border: 1px solid #F8B3CC;font-size : 14px;float: none; ">' . $checkCrmmobSubscription['message'] . '</div>';
            }
        } else {
            if (!empty($checkCrmmobSubscription['message'])) {
                echo '<div style="color: #f11147;font-size: 14px;left: 0;text-align: center;top: 50%;float: none; ">' . $checkCrmmobSubscription['message'] . '</div>';
            }
            $smarty = new Sugar_Smarty();
            $smarty->display('modules/ModuleBuilder/tpls/includes.tpl');
            global $app_list_strings;
            echo "<h1>Module Selection for User Accessible Modules </h1>";

            parent::display();

            global $current_user;
            $i = isset($_REQUEST['i']) ? $_REQUEST['i'] : '';
            $r = isset($_REQUEST['setting']) ? $_REQUEST['setting'] : '';
            $unique_id = create_guid();
            $html = '';
            global $db;

            $html .= "<br/><p align='center' id='label_msg' style='font-weight:bold; color:green;'> </p><br/>";

            $html .= "<input type='button' id='sel' onclick='sel()' value='Select All' />&nbsp;<input type='button' id='dsel' onclick='dsel()' value='Deselect All' /><br/><br/>";
            $html .= "<input type='hidden' id='submit_type' value='insert' /><table width='100%' border=1 class='formHeader h3Row' style='border-bottom: 1px solid #abc3d7;'><tr><th height='40' style='border-bottom: 1px solid #abc3d7;'><h4>Modules</h4></th></tr>";

            $controller = new TabController();
            $tabs = $controller->get_tabs_system();

            $enabled = array();

            $query = "SELECT * FROM user_accessible_module";
            $result = $db->query($query);
            //   $f = 0;
            while ($row = $db->fetchByassoc($result)) {
                if ($row['id'] != '' || $row['setting'] != '') {
                    $available_modules = explode(',', $row['setting']);
                }
            }

            $html .= "<tr><td>";
            foreach ($tabs[0] as $key => $value) {
                if (file_exists('modules/' . $key . '/metadata/studio.php') && isset($GLOBALS ['beanList'][$key])) {
                if (in_array($key, $available_modules)) {
                    $html .= "<div style='margin:9px;'><input type='checkbox' checked='checked' value='$key' class='my'/>    " . $app_list_strings['moduleList'][$value] . "</div>    ";
                } else {
                    $html .= "<div style='margin:9px;'><input type='checkbox' value='$key' class='my'/>    " . $app_list_strings['moduleList'][$value] . "</div>    ";
                }
            }
            }
            $html .= "</td></tr>";
            $html .= "</table><br/><input type='submit' value='Save' onclick='savemodules(\"$unique_id\")'/>&nbsp;";
            $html .= "<input type='button' name='cancel' onclick='document.location.href=\"index.php?module=Administration&action=index\"' value='Cancel' />";

            echo $html;
        }
    }

}
?>
<script type="text/javascript">
    function sel() {
        $('.my').prop('checked', 'checked');
    }
    function dsel() {
        $('.my').removeAttr('checked');
    }
    function savemodules(id) {
        var selectedModules = new Array();
        var submit_typeVal = $('#submit_type').val();
        var alertMsg = 'You have to select atleast single module before save.';
        var notify_type = 'alert';
        if (submit_typeVal == 'update') {
            notify_type = 'confirm';
            alertMsg = 'You are going to reset your accessible modules.Do you want to proceed ?';
        }
        $(".my").each(function () {
            if ($(this).is(':checked')) {
                selectedModules.push($(this).val())
            }
        });
        var selectedModulesString = JSON.stringify(selectedModules);
        var proceedToSave = true;
        if (selectedModules == '') {
            if (notify_type == 'alert') {
                proceedToSave = false;
                alert(alertMsg);
            } else {
                if (confirm(alertMsg)) {
                    proceedToSave = true;
                } else {
                    proceedToSave = false;
                }
            }
        }
        $('#label_msg').text('');
        if (proceedToSave) {
        $.ajax({
            url: 'index.php',
            type: 'POST',
                data: {module: 'Administration', action: 'CRMMobHandler', id: id, setting: selectedModulesString, method: 'savemodules'},
            success: function (data) {
                    $('#submit_type').val('update');
                    if (data == 'insert') {
                        $('#label_msg').text('Module(s) saved successfully.').show();
                    } else {
                        $('#label_msg').text('Module(s) updated successfully.').show();
                }
            },
            error: function (msg)
            {
                alert("Save Error  :  " + msg);
            }
        });
    }
    }

</script>