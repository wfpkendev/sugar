<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
class AdministrationViewLicence extends SugarView
{
    var $PluginConfig;
    function display()
    {
        include 'custom/PluginConfiguration/config.php';
        $this->PluginConfig = $PluginConfig;
        $plugin_name = $this->PluginConfig['plugin_name'];
        global $sugar_version;
        require_once('modules/Administration/Administration.php');
        $administrationObj = new Administration();
        $administrationObj->retrieveSettings($plugin_name);
        $LastValidation = $administrationObj->settings[$plugin_name.'_LastValidation'];
        $ModuleEnabled = $administrationObj->settings[$plugin_name.'_ModuleEnabled'];
        $licenseKey = (!empty($administrationObj->settings[$plugin_name.'_LicenceKey'])) ? $administrationObj->settings[$plugin_name.'_LicenceKey'] : "";
        $is_user_base = $administrationObj->settings[$plugin_name.'_UserBase'];
        if($is_user_base){
            $AllowedUserCount = $administrationObj->settings[$plugin_name.'_AllowedUserCount'];
            $used_lic_count = $administrationObj->settings[$plugin_name.'_LicUserCount'];
            $remaining_lic_count = $AllowedUserCount - $used_lic_count;
        }

        $re_sugar_version= '/(6\.4\.[0-9])/';
        if (preg_match($re_sugar_version, $sugar_version)) {
            echo '<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>';
            echo '<script type="text/javascript" src="http://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>';
            echo '<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />';
        }

        $display_back = "display:none;";
        if ($LastValidation == 0) {
            $display_back = "display:block;";
        }
        $OnClickEvent = "onclick=enablePlugin();";
        if(isset($AllowedUserCount) && $AllowedUserCount != '' && isset($is_user_base)){
            require_once('include/templates/TemplateGroupChooser.php');
            $chooser = new TemplateGroupChooser();

            $chooser->args['left_name'] = 'unlicensed_users';
            $chooser->args['right_name'] = 'licensed_users';
            $chooser->args['left_label'] =  'Unlincensed User';
            $chooser->args['right_label'] =  'Licenced User';
            $chooser->args['id'] = 'edit_licensed_users';
            $chooser->args['values_array'] = array();
            $chooser->args['values_array'][0] = get_user_array(false, 'Active', '', false, '', " AND is_group=0");
            $chooser->args['values_array'][1] = array();
            $chooser->args['left_size'] = count($chooser->args['values_array'][0]);
            $chooser->args['right_size'] = count($chooser->args['values_array'][0]);
            $used_licenses = 0;
            $allocated_lic_users = array();
            if(!empty($administrationObj->settings[$plugin_name.'_LicUsers'])){
                $allocated_lic_users = explode(',',$administrationObj->settings[$plugin_name.'_LicUsers']);
            }
            if(count($allocated_lic_users) > 0){
                foreach($allocated_lic_users as $allocated_user)
                {
                    $used_licenses++;
                    $oUser = new User;
                    $oUser->retrieve($allocated_user);
                    $display_name = $oUser->name;
                    $chooser->args['values_array'][1][$allocated_user] = $display_name;
                    unset($chooser->args['values_array'][0][$allocated_user]);
                }
            }


			$user_management = $chooser->display();
            $this->ss->assign('user_management',$user_management);
            $OnClickEvent = "onclick=enableUserBasePlugin();";
        }


        $this->ss->assign('plugin_name',$plugin_name);
        $this->ss->assign('LastValidation',$LastValidation);
        $this->ss->assign('licenseKey',$licenseKey);
        $this->ss->assign('display_back',$display_back);
        $this->ss->assign('ModuleEnabled',$ModuleEnabled);
        $this->ss->assign('AllowedUserCount',$AllowedUserCount);
        $this->ss->assign('remaining_lic_count',$remaining_lic_count);
        $this->ss->assign('OnClickEvent',$OnClickEvent);
        $this->ss->assign('is_user_base',$is_user_base);
        $this->ss->display('custom/Licence/tpl/licence.tpl');
        parent::display();
    }
}

?>

