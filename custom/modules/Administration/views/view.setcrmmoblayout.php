<?php

require_once ('modules/ModuleBuilder/parsers/ParserFactory.php');
require_once ('modules/ModuleBuilder/MB/AjaxCompose.php');
require_once 'modules/ModuleBuilder/parsers/constants.php';

Class AdministrationViewSetcrmmobLayout extends SugarView {

    function display() {
        global $sugar_config, $current_user, $db, $app_list_strings;
        parent::display();
        require_once('custom/Licence/classes/Licenceutils.php');
        $checkCrmmobSubscription = Licenceutils::validateCRMMobSubscription();
        if (!$checkCrmmobSubscription['success']) {
            if (!empty($checkCrmmobSubscription['message'])) {
                echo '<div style="color: #F11147;text-align: center;background: #FAD7EC;padding: 10px;margin: 3% auto;width: 70%;top: 50%;left: 0;right: 0;border: 1px solid #F8B3CC;font-size : 14px;">' . $checkCrmmobSubscription['message'] . '</div>';
            }
        } else {
            if (!empty($checkCrmmobSubscription['message'])) {
                echo '<div style="color: #f11147;font-size: 14px;left: 0;text-align: center;top: 50%;">' . $checkCrmmobSubscription['message'] . '</div>';
            }
            $smarty = new Sugar_Smarty();
            $smarty->display('modules/ModuleBuilder/tpls/includes.tpl');

            /* for include suitecrm theme */
            $re_suite_version = '/(7\.?(\d+\.)?(\*|\d+)$)/';
            if ($sugar_config['suitecrm_version'] != '' && preg_match($re_suite_version, $sugar_config['suitecrm_version'])) {
                $current_theme = $current_user->getPreference('user_theme');
                $random_num = mt_rand();
                $file = "custom/biz/css/{$current_theme}/suiteR_portal_style.css";
                if (file_exists($file) && $current_theme == 'SuiteR') {
                    echo "<link href='{$file}?{$random_num}' rel='stylesheet'>";
                } else {
                    echo "<link href='custom/biz/css/Suite7/suite7_portal_style.css?{$random_num}' rel='stylesheet'>";
                }
            } else {
                echo "<link rel='stylesheet' href='custom/biz/css/portal_style.css' type='text/css'>";
            }

            echo "<script type='text/javascript' src='custom/biz/js/layout-set.js'></script>";


            $query = "SELECT * FROM user_accessible_module";
            $result = $db->query($query);
            while ($row = $db->fetchByassoc($result)) {
                $modules = explode(',', $row['setting']);
            }
            $html = '';
            if (!empty($_REQUEST['msg']) && $_REQUEST['msg'] == 'success') {
                $html .= "<div style='margin:10px;text-align:center'><span style='color:green;font-weight:bold;'> Changes saved successfully.</span></div>";
            }
            $html = "<form name='setLayout' class='module-form'>";
            $html .= "<div class='title'><h1>Mobile Layout Setting</h1></div>";
            $html .= "<div class='module-selection'><label>Select Modules: </label><select name='modules' id='modules'>
                    <option value=''>Select</option> ";

            // set modules as per role
            $userAccessibleModulesList = query_module_access_list($current_user);
            foreach ($modules as $module) {
                if (in_array($module, $userAccessibleModulesList)) {
                    $access_right_modules[] = $module;
            }
            }
            foreach ($access_right_modules as $module) {
                $html .= "<option value='{$module}'>{$app_list_strings['moduleList'][$module]}</option>";
            }
            $html .= " </select></div>";
            $html .= "<div class='btn-bar'>
                        <input type='button' class='blue-btn' name='save' value='Save' class='button' onclick='saveModulelayout();'>
                        <input type='button' class='blue-btn' name='cancel' value='Cancel' class='button' onclick='redirectToindex();'>
                 </div></form>";
            echo $html;
        }
        echo " <script type='text/javascript'>
                $('document').ready(function(){
                    var panel_index = 0;
                    $('.list-layout-block').css('display','none');
                    $('#modules').change(function(){

                        $('<input>').attr({
                            type: 'hidden',
                            id: 'layout_type',
                            name: 'layout_type'
                        }).appendTo('form');
                        $('#layouts').html('');

                        var module = $('#modules').val();
                        $('#modules').after('<div id=\"layouts\" class=\"layouts\"  style=\"margin:10px\"></div>');
                        $('#layouts').append('<div class=\"edit-view\"><a id=\"editview\" onclick=\"get_fields('+\"'\" + module+\"'\"+ ','+\"'edit'\"+')\">Edit View</a></div>' +
                                             '<div class=\"detail-view\"><a id=\"detailview\" onclick=\"get_fields('+\"'\" + module+\"'\"+ ','+\"'detail'\"+')\">Detail View</a></div>' +
                                             '<div class=\"list-view\"><a  id=\"listview\" onclick=\"get_list_fields('+\"'\" + module+\"'\"+ ','+\"'list'\"+')\">List View</a></div>' +
                                             '<div class=\"list-layout-block\"><div id=\"fieldlist\" style=\"float:left\"></div></div>') ;
                         $('.list-layout-block').css('display','none');
                    });
                });
            
        </script>";
    }

}
