<?php
require_once 'custom/biz/classes/CRMMobController.php';
$oPortalcontroller = new CRMMobController();
if(!empty($_REQUEST['method']))
{
    $method = $_REQUEST['method'];
    switch ($method) {
        case 'getModuleFields':
            $result = $oPortalcontroller->getModuleFields();
            break;
        case 'saveListLayout':
            $result = $oPortalcontroller->saveListLayout();
            break;
        case 'saveEditLayout':
            $result = $oPortalcontroller->saveEditLayout();
            break;
        case 'setWPUrl':
            $result = $oPortalcontroller->setWPUrl();
            break;
        case 'checkExistEmail':
            $result = $oPortalcontroller->checkExistEmail();
            break;
        case 'savemodules':
            $result = $oPortalcontroller->SaveUserAccessibleModules();
        default:
            break;
    }
}
else
{
    echo 'not valid action';
}
ob_clean();    
echo json_encode($result);
die();