<?php
include 'custom/PluginConfiguration/config.php';
require_once 'custom/Licence/classes/LicenceController.php';
$licencecontroller = new LicenceController();
$licencecontroller->PluginConfig = $PluginConfig;
if(!empty($_REQUEST['method']))
{
    $method = $_REQUEST['method'];
    switch ($method) {
        case 'validateLicence':
            $result = $licencecontroller->validateLicence();
            break;
        case 'enableDisableModules':
            $result = $licencecontroller->enableDisableModules();
            break;
        default:
            break;
    }
}
else
{
    echo 'not valid action';
}
ob_clean();    
echo json_encode($result);
die();