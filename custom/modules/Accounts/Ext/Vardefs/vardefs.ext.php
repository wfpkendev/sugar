<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2014-04-24 16:27:21
$dictionary['Account']['fields']['account_type']['default']='Beneficiary';
$dictionary['Account']['fields']['account_type']['len']=100;
$dictionary['Account']['fields']['account_type']['required']=true;
$dictionary['Account']['fields']['account_type']['comments']='The account holder is of this type';
$dictionary['Account']['fields']['account_type']['merge_filter']='disabled';

 

 // created: 2013-11-18 20:57:20

 

 // created: 2013-11-18 20:58:23

 

 // created: 2016-02-09 11:03:15
$dictionary['Account']['fields']['house_hold_number_c']['labelValue']='Household number';

 

 // created: 2016-02-09 10:54:23
$dictionary['Account']['fields']['name']['required']=false;
$dictionary['Account']['fields']['name']['comments']='Name of the Complainant';
$dictionary['Account']['fields']['name']['importable']='true';
$dictionary['Account']['fields']['name']['merge_filter']='disabled';

 
?>