<?php
// created: 2014-04-24 16:32:01
$mod_strings = array (
  'LBL_ACCOUNT' => 'Account',
  'LBL_ACCOUNT_DESCRIPTION' => 'Account Description',
  'LBL_ACCOUNT_ID' => 'Account ID',
  'LBL_ACCOUNT_NAME' => 'Account Name:',
  'LBL_LIST_ACCOUNT_NAME' => 'Account Name',
  'LBL_CONVERTED_ACCOUNT' => 'Converted Account:',
  'LNK_SELECT_ACCOUNTS' => ' OR Select Account',
  'LNK_NEW_ACCOUNT' => 'Create Account',
);