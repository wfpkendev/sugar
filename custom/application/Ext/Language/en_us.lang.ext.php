<?php 
 //WARNING: The contents of this file are auto-generated



include 'custom/PluginConfiguration/config.php';
$plugin_name = $PluginConfig['plugin_name'];
$app_strings['VALIDATE_ERROR'] = 'There seems some error while validating your license for ' . $plugin_name . ' module. Please try again later.';
$app_strings['VALIDATE_FAIL'] = 'Please contact your Administrator to validate License.';
$app_strings['USER_VALIDATE_FAIL'] = 'You are not authorized user for this Application.Please contact your Administrator to authorize you.';


/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


$app_list_strings['moduleList']['field_Helpdesk'] = 'Helpdesk ';


$app_list_strings['moduleList']['zr2_ReportParameter'] = 'Report Parameter';
$app_list_strings['moduleList']['zr2_ReportTemplate'] = 'JasperReports Template';
$app_list_strings['moduleList']['zr2_QueryTemplate'] = 'Query Template';
$app_list_strings['moduleList']['zr2_ReportParameterLink'] = 'Parameter Bindings';
$app_list_strings['moduleList']['zr2_Report'] = 'ZuckerReports';
$app_list_strings['moduleList']['zr2_ReportContainer'] = 'Report Archive';

$app_list_strings['PARAM_RANGE_TYPES']['SIMPLE'] = 'Direct Input';
$app_list_strings['PARAM_RANGE_TYPES']['DATE'] = 'Date Input';
$app_list_strings['PARAM_RANGE_TYPES']['DATE_NOW'] = 'Current Time and Date';
$app_list_strings['PARAM_RANGE_TYPES']['DATE_ADD'] = 'Future Timestamp';
$app_list_strings['PARAM_RANGE_TYPES']['DATE_SUB'] = 'Past Timestamp';
$app_list_strings['PARAM_RANGE_TYPES']['SQL'] = 'User-Defined Query';
$app_list_strings['PARAM_RANGE_TYPES']['LIST'] = 'User-Defined List';
$app_list_strings['PARAM_RANGE_TYPES']['DROPDOWN'] = 'Drop-Down List';
$app_list_strings['PARAM_RANGE_TYPES']['CURRENT_USER'] = 'Current User';
$app_list_strings['PARAM_RANGE_TYPES']['SCRIPT'] = 'PHP Script';
$app_list_strings['PARAM_DATE_TYPES']['MINUTE'] = 'Minute(s)';
$app_list_strings['PARAM_DATE_TYPES']['HOUR'] = 'Hour(s)';
$app_list_strings['PARAM_DATE_TYPES']['DAY'] = 'Day(s)';
$app_list_strings['PARAM_DATE_TYPES']['WEEK'] = 'Week(s)';
$app_list_strings['PARAM_DATE_TYPES']['MONTH'] = 'Month(s)';
$app_list_strings['PARAM_DATE_TYPES']['YEAR'] = 'Year(s)';



?>