<?php 
 //WARNING: The contents of this file are auto-generated

 
 //WARNING: The contents of this file are auto-generated
$beanList['field_Helpdesk'] = 'field_Helpdesk';
$beanFiles['field_Helpdesk'] = 'modules/field_Helpdesk/field_Helpdesk.php';
$moduleList[] = 'field_Helpdesk';




/* * *******************************************************************************
 * This file is part of KReporter. KReporter is an enhancement developed
 * by Christian Knoll. All rights are (c) 2012 by Christian Knoll
 *
 * This Version of the KReporter is licensed software and may only be used in
 * alignment with the License Agreement received with this Software.
 * This Software is copyrighted and may not be further distributed without
 * witten consent of Christian Knoll
 *
 * You can contact us at info@kreporter.org
 * ****************************************************************************** */

$GLOBALS['sugar_config']['addAjaxBannedModules'][] = 'KReports';


 
 //WARNING: The contents of this file are auto-generated
$beanList['zr2_Report'] = 'zr2_Report';
$beanFiles['zr2_Report'] = 'modules/zr2_Report/zr2_Report.php';
$moduleList[] = 'zr2_Report';
$beanList['zr2_ReportContainer'] = 'zr2_ReportContainer';
$beanFiles['zr2_ReportContainer'] = 'modules/zr2_ReportContainer/zr2_ReportContainer.php';
$modules_exempt_from_availability_check['zr2_ReportContainer'] = 'zr2_ReportContainer';
$report_include_modules['zr2_ReportContainer'] = 'zr2_ReportContainer';
$modInvisList[] = 'zr2_ReportContainer';
$beanList['zr2_ReportParameter'] = 'zr2_ReportParameter';
$beanFiles['zr2_ReportParameter'] = 'modules/zr2_ReportParameter/zr2_ReportParameter.php';
$modules_exempt_from_availability_check['zr2_ReportParameter'] = 'zr2_ReportParameter';
$report_include_modules['zr2_ReportParameter'] = 'zr2_ReportParameter';
$modInvisList[] = 'zr2_ReportParameter';
$beanList['zr2_ReportParameterLink'] = 'zr2_ReportParameterLink';
$beanFiles['zr2_ReportParameterLink'] = 'modules/zr2_ReportParameterLink/zr2_ReportParameterLink.php';
$modules_exempt_from_availability_check['zr2_ReportParameterLink'] = 'zr2_ReportParameterLink';
$report_include_modules['zr2_ReportParameterLink'] = 'zr2_ReportParameterLink';
$modInvisList[] = 'zr2_ReportParameterLink';
$beanList['zr2_ReportTemplate'] = 'zr2_ReportTemplate';
$beanFiles['zr2_ReportTemplate'] = 'modules/zr2_ReportTemplate/zr2_ReportTemplate.php';
$modules_exempt_from_availability_check['zr2_ReportTemplate'] = 'zr2_ReportTemplate';
$report_include_modules['zr2_ReportTemplate'] = 'zr2_ReportTemplate';
$modInvisList[] = 'zr2_ReportTemplate';
$beanList['zr2_QueryTemplate'] = 'zr2_QueryTemplate';
$beanFiles['zr2_QueryTemplate'] = 'modules/zr2_QueryTemplate/zr2_QueryTemplate.php';
$modules_exempt_from_availability_check['zr2_QueryTemplate'] = 'zr2_QueryTemplate';
$report_include_modules['zr2_QueryTemplate'] = 'zr2_QueryTemplate';
$modInvisList[] = 'zr2_QueryTemplate';


?>