<?php

require_once('service/v4_1/registry.php');

class registry_v4_1_custom extends registry_v4_1
{
    protected function registerFunction()
    {
        parent::registerFunction();

        $this->serviceClass->registerFunction(
            'get_customListlayout',
            array('session'=>'xsd:string', 'module_name'=>'xsd:string', 'view'=>'xsd:string'),
            array('return'=>'tns:list_layout'));
        $this->serviceClass->registerFunction(
            'get_allCustomLayouts',
            array('session'=>'xsd:string', 'modules'=>'tns:select_fields'),
            array('return'=>'tns:list_layout'));
        $this->serviceClass->registerFunction(
            'get_dashBoardRecords',
            array('session'=>'xsd:string', 'modules'=>'tns:select_fields'),
            array('return'=>'tns:records'));
        $this->serviceClass->registerFunction(
            'user_generate_password',
            array('session'=>'xsd:string', 'user_info'=>'tns:users_fields'),
            array('return'=>'tns:records'));
        $this->serviceClass->registerFunction(
            'user_reset_password',
            array('session'=>'xsd:string', 'user_info'=>'tns:users_fields'),
            array('return'=>'tns:records'));
        $this->serviceClass->registerFunction(
            'get_mod_fields',
            array('session'=>'xsd:string', 'modules'=>'tns:select_fields'),
            array('return'=>'tns:records'));

        $this->serviceClass->registerFunction(
            'getUser_accessibleModules',
            array('session'=>'xsd:string', 'module_name'=>'xsd:string','user_id'=>'xsd:string'),
            array('return'=>'tns:access_right_modules'));

        $this->serviceClass->registerFunction(
            'getUserTimezone',
            array('session'=>'xsd:string', 'module_name'=>'xsd:string','user_id'=>'xsd:string'),
            array('return'=>'tns:timezone'));

        $this->serviceClass->registerFunction(
            'getDynamicOptions',
            array('session'=>'xsd:string', 'module_name'=>'xsd:string'),
            array('return'=>'tns:ParentDetails'));
        $this->serviceClass->registerFunction(
            'user_generate_password',
            array('user_info'=>'tns:users_fields'),
            array('return'=>'tns:records'));
        $this->serviceClass->registerFunction(
            'user_reset_password',
            array('session'=>'xsd:string', 'user_info'=>'tns:users_fields'),
            array('return'=>'tns:records'));
        $this->serviceClass->registerFunction(
            'validateLicence',
            array('user_name' => 'xsd:string'),
            array('return'=>'tns:LicenceDetails'));
    }


}

?>