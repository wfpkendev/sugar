<?php

if (!defined('sugarEntry'))
    define('sugarEntry', true);
require_once('service/v4_1/SugarWebServiceImplv4_1.php');
// Change on 13-05-2016: Resolve issue for Role and User Profile update.
require_once('custom/service/v4_1_custom/SugarWebServiceUtilv4_1_custom.php');
// End
require_once 'modules/ModuleBuilder/parsers/views/GridLayoutMetaDataParser.php';

class SugarWebServiceImplv4_1_custom extends SugarWebServiceImplv4_1 {
// Change on 13-05-2016: Resolve issue for Role and User Profile update.
    public function __construct() {
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
    }
// End
    function validateLicence($user_name) {

        require_once('modules/Administration/Administration.php');
        require_once('custom/Licence/classes/Licenceutils.php');
        include 'custom/PluginConfiguration/config.php';
        $plugin_name = $PluginConfig['plugin_name'];
        global $app_strings, $db;
        $administrationObj = new Administration();
        $administrationObj->retrieveSettings($plugin_name);
        $licencekey = $administrationObj->settings[$plugin_name.'_LicenceKey'];
        $lastValidation = $administrationObj->settings[$plugin_name.'_LastValidation'];
        $currDate = date("Y/m/d");
        $nextValidationDate = $administrationObj->settings[$plugin_name.'_NextValidationDate'];
        $LicUsers = explode(',', $administrationObj->settings[$plugin_name.'_LicUsers']);
        $query = "SELECT id FROM users WHERE user_name = '{$user_name}' AND deleted = 0";
        $result = $db->query($query);
        $rows = $db->fetchByAssoc($result);
        $user_id = $rows['id'];

        if (in_array($user_id, $LicUsers)) {
            if ($licencekey && $lastValidation && $nextValidationDate) {
                if (strtotime($currDate) > strtotime($nextValidationDate)) {
                    $CheckResult = Licenceutils::checkPluginLicence($licencekey);
                    if ($CheckResult) {
                        $validateModuleEnable = Licenceutils::validateCRMMobSubscription();
                        if ($validateModuleEnable['success']) {
                            return array('success' => 1, 'message' => '');
                        } else {
                            return array('success' => 0, 'message' => $validateModuleEnable['message']);
                        }
                    } else {
                        return array('success' => 0, 'message' => $app_strings['VALIDATE_ERROR']);
                    }
                } else {
                    $validateModuleEnable = Licenceutils::validateCRMMobSubscription();
                    if ($validateModuleEnable['success']) {
                        return array('success' => 1, 'message' => '');
                    } else {
                        return array('success' => 0, 'message' => $validateModuleEnable['message']);
                    }
                }
            } else {
                return array('success' => 0, 'message' => $app_strings['VALIDATE_FAIL']);
            }
        } else {
            return array('success' => 0, 'message' => $app_strings['USER_VALIDATE_FAIL']);
        }
    }

    function get_customListlayout($session, $module_name, $view) {
        global $db;
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $module_name, 'read', 'no_access', $error)) {
            $GLOBALS['log']->info('End: checkEmailAndSendUsername->getCasesRelatedToContact');
            return;
        }
        $targetModuleLang = return_module_language('en_us', $module_name);
        if ($view == 'list') {
            $check_existance_query = "select layout from portal_layout where module_name='$module_name' and layout_type='list'";
            $getResult = $db->query($check_existance_query);
            $savedDblayoutArr = $db->fetchByAssoc($getResult);
            $layoutSaved = html_entity_decode($savedDblayoutArr['layout']);
            $savedlistLayoutData = json_decode($layoutSaved, true);
            try {
                if (!empty($savedlistLayoutData)) {
                    return $savedlistLayoutData;
                } else {
                    throw new Exception("Portal ListView layout is not set for {$module_name}.");
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
        if ($view == 'edit') {
            $check_existance_query = "select layout from portal_layout where module_name='$module_name' and layout_type='edit'";
            $getResult = $db->query($check_existance_query);
            $savedDblayoutArr = $db->fetchByAssoc($getResult);
            $layoutSaved = html_entity_decode($savedDblayoutArr['layout']);
            $savededitLayoutData = json_decode($layoutSaved, true);
            try {
                if (!empty($savededitLayoutData)) {
                    // Add extra panel to add Quote line items. 
                    if ($module_name == 'AOS_Quotes' || $module_name = 'AOS_Invoices') {
                        $savededitLayoutData[$module_name]['EditView']['panels']['LBL_DEFAULT_LINE_ITEMS'] = array(
                            'lable_value' => $targetModuleLang['LBL_DEFAULT_LINE_ITEMS'], 'rows' => $this->getLineItemRows($module_name),
                        );
                    }
                    return $savededitLayoutData;
                } else {
                    throw new Exception("Portal EditView layout is not set for {$module_name}.");
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
        if ($view == 'detail') {
            $check_existance_query = "select layout from portal_layout where module_name='$module_name' and layout_type='detail'";
            $getResult = $db->query($check_existance_query);
            $savedDblayoutArr = $db->fetchByAssoc($getResult);
            $layoutSaved = html_entity_decode($savedDblayoutArr['layout']);
            $saveddetailLayoutData = json_decode($layoutSaved, true);
            try {
                if (!empty($saveddetailLayoutData)) {
                    // Add extra panel to add Quote line items. 
                    if ($module_name == 'AOS_Quotes' || $module_name = 'AOS_Invoices') {
                        $saveddetailLayoutData[$module_name]['DetailView']['panels']['LBL_DEFAULT_LINE_ITEMS'] = array(
                            'lable_value' => $targetModuleLang['LBL_DEFAULT_LINE_ITEMS'], 'rows' => $this->getLineItemRows($module_name),
                        );
                    }
                    return $saveddetailLayoutData;
                } else {
                    throw new Exception("Portal DetailView layout is not set for {$module_name}.");
                }
            } catch (Exception $e) {
                return $e->getMessage();
            }
        }
    }

    /**
     * Description : This function returns module's all custom layouts which are
     * used in mobile application to render the data.
     * @param string $module_name
     * @param string $lastUpdateDate
     * @param string $newDate
     * @param array $view
     * 
     * @return array 
     */
    function get_ModuleLayouts($module_name, $lastUpdateDate, $newDate, $view) {
        global $db;
        $portalViewDefs = array();
        $targetModuleLang = return_module_language('en_us', $module_name);
        //for listview
        if (in_array('list', $view)) {
            $check_existance_query = "select layout from portal_layout where module_name='$module_name' and layout_type='list'";
            $getResult = $db->query($check_existance_query);
            $savedDblayoutArr = $db->fetchByAssoc($getResult);
            $layoutSaved = html_entity_decode($savedDblayoutArr['layout']);
            $savedlistLayoutData = json_decode($layoutSaved, true);
            if (!empty($savedlistLayoutData)) {
                if (empty($lastUpdateDate) ||
                        strtotime($lastUpdateDate) < strtotime($savedlistLayoutData[$module_name]['updated_date'])) {
                    $portalViewDefs['listviewdefs'] = $savedlistLayoutData[$module_name];
                }

                // Change updated date while new request for the lauout setting.
                // added layout updated date to the file.
                $savedlistLayoutData['updated_date'] = $newDate;
                $layoutContent = json_encode($savedlistLayoutData);
                $unique_id = create_guid();
                // get fields from db table
                $check_existance_query = "select * from portal_layout where module_name='$module_name' and layout_type='list'";
                $getResult = $db->query($check_existance_query);
                if ($getResult->num_rows == 0) {
                    //insert new record to db
                    $save_listview_query = "Insert into portal_layout values('$unique_id','$module_name','list','$layoutContent')";
                    $result = $db->query($save_listview_query);
                } else {
                    //update existing record to db
                    $save_listview_query = "Update portal_layout set layout='$layoutContent' where module_name='$module_name' and layout_type='list'";
                    $result = $db->query($save_listview_query);
                }
                // end update date.
            }
        }
        // for editview
        if (in_array('edit', $view)) {
            $check_existance_query = "select layout from portal_layout where module_name='$module_name' and layout_type='edit'";
            $getResult = $db->query($check_existance_query);
            $savedDblayoutArr = $db->fetchByAssoc($getResult);
            $layoutSaved = html_entity_decode($savedDblayoutArr['layout']);
            $savededitLayoutData = json_decode($layoutSaved, true);
            if (!empty($savededitLayoutData)) {
                // Add extra panel to add Quote line items. 
                if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices') {
                    $savededitLayoutData[$module_name]['EditView']['panels']['LBL_DEFAULT_LINE_ITEMS'] = array(
                        'lable_value' => $targetModuleLang['LBL_DEFAULT_LINE_ITEMS'], 'rows' => $this->getLineItemRows($module_name),
                    );
                }
                if (empty($lastUpdateDate) ||
                        strtotime($lastUpdateDate) < strtotime($savededitLayoutData[$module_name]['EditView']['updated_date'])) {
                    $portalViewDefs['editviewdefs'] = $savededitLayoutData[$module_name]['EditView'];
                }
                // Change updated date while new request for the lauout setting.

                $savededitLayoutData[$module_name]['EditView']['updated_date'] = $newDate;
                // added layout updated date to the file.
                $layoutContent = json_encode($savededitLayoutData);
                $unique_id = create_guid();
                // get fields from db table
                $check_existance_query = "select * from portal_layout where module_name='$module_name' and layout_type='edit'";
                $getResult = $db->query($check_existance_query);
                if ($getResult->num_rows == 0) {
                    //insert new record to db
                    $save_listview_query = "Insert into portal_layout values('$unique_id','$module_name','edit','$layoutContent')";
                    $result = $db->query($save_listview_query);
                } else {
                    //update existing record to db
                    $save_listview_query = "Update portal_layout set layout='$layoutContent' where module_name='$module_name' and layout_type='edit'";
                    $result = $db->query($save_listview_query);
                }
                // end updated date
            }
        }
        // For DetailView 
        if (in_array('detail', $view)) {
            $check_existance_query = "select layout from portal_layout where module_name='$module_name' and layout_type='detail'";
            $getResult = $db->query($check_existance_query);
            $savedDblayoutArr = $db->fetchByAssoc($getResult);
            $layoutSaved = html_entity_decode($savedDblayoutArr['layout']);
            $saveddetailLayoutData = json_decode($layoutSaved, true);
            if (!empty($saveddetailLayoutData)) {
                // Add extra panel to add Quote line items. 
                if ($module_name == 'AOS_Quotes' || $module_name == 'AOS_Invoices') {
                    $saveddetailLayoutData[$module_name]['DetailView']['panels']['LBL_DEFAULT_LINE_ITEMS'] = array(
                        'lable_value' => $targetModuleLang['LBL_DEFAULT_LINE_ITEMS'], 'rows' => $this->getLineItemRows($module_name),
                    );
                }
                if (empty($lastUpdateDate) ||
                        strtotime($lastUpdateDate) < strtotime($saveddetailLayoutData[$module_name]['DetailView']['updated_date'])) {
                    $portalViewDefs['detailviewdefs'] = $saveddetailLayoutData[$module_name]['DetailView'];
                }
                // Change updated date while new request for the lauout setting.
                // added layout updated date to the file.
                $saveddetailLayoutData[$module_name]['DetailView']['updated_date'] = $newDate;
                $layoutContent = json_encode($saveddetailLayoutData);
                $unique_id = create_guid();
                // get fields from db table
                $check_existance_query = "select * from portal_layout where module_name='$module_name' and layout_type='detail'";
                $getResult = $db->query($check_existance_query);
                if ($getResult->num_rows == 0) {
                    //insert new record to db
                    $save_listview_query = "Insert into portal_layout values('$unique_id','$module_name','detail','$layoutContent')";
                    $result = $db->query($save_listview_query);
                } else {
                    //update existing record to db
                    $save_listview_query = "Update portal_layout set layout='$layoutContent' where module_name='$module_name' and layout_type='detail'";
                    $result = $db->query($save_listview_query);
                }
                //end update date.
            }
        }
        return $portalViewDefs;
    }

    function get_allCustomLayouts($session, $modules) {
        global $timedate;
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
        $allModulesLayouts = array();
        $views = array('list', 'edit', 'detail');
        $newDate = $timedate->nowDb();
        $lastUpdateDate = (!empty($modules['last_sync'])) ? $modules['last_sync'] : "";

        try {
            if (!empty($modules['modules'])) {
                $allModulesLayouts['sync_on'] = $newDate;
                foreach ($modules['modules'] as $moduleName) {
                    $error = new SoapError();
                    if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $moduleName, 'read', 'no_access', $error)) {
                        $GLOBALS['log']->info('End: checkEmailAndSendUsername->getCasesRelatedToContact');
                        return;
                    }

                    $aLayoutDefs = $this->get_ModuleLayouts($moduleName, $lastUpdateDate, $newDate, $views);

                    if (!empty($aLayoutDefs)) {
                        $allModulesLayouts['modules'][$moduleName] = $aLayoutDefs;
                    }
                }
            }
            return $allModulesLayouts;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    function get_dashBoardRecords($session, $modules) {
        $max_records = (isset($modules['max_records'])) ? $modules['max_records'] : 5;
        $dashBoard_Data = array();
        if (!empty($modules['modules'])) {
            foreach ($modules['modules'] as $module_name) {
                $error = new SoapError();
                if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $moduleName, 'read', 'no_access', $error)) {
                    $GLOBALS['log']->info('End: checkEmailAndSendUsername->getCasesRelatedToContact');
                    return;
                }
                $dashBoard_Data[$module_name] = $this->get_entry_list($session, $module_name, '', 'date_modified DESC', '', '', '', $max_records - 1, 0);
            }
        }

        return $dashBoard_Data;
    }

    function get_mod_fields($session, $modules) {
        $modulesFields = array();
        if (!empty($modules['modules'])) {
            foreach ($modules['modules'] as $module_name) {
                $error = new SoapError();
                if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $moduleName, 'read', 'no_access', $error)) {
                    $GLOBALS['log']->info('End: checkEmailAndSendUsername->getCasesRelatedToContact');
                    return;
                }
                $modulesFields[$module_name] = $this->get_module_fields($session, $module_name);
            }
        }
        return $modulesFields;
    }

    /**
     * Description : This function returns array of rows which are used in Quote Line item and Invoce Line items.
     * @param string $module 
     * @return array 
     */
    function getLineItemRows($module) {
        global $beanList, $app_list_strings;
        $moduleObj = new $beanList[$module]();
        $lineItemFields = array('total_amt', 'subtotal_amount', 'discount_amount', 'tax_amount', 'shipping_amount', 'shipping_tax', 'shipping_tax_amt', 'total_amount', 'subtotal_tax_amount');

        $fieldDef = $moduleObj->getFieldDefinitions();

        $panelRows = array();

        $row_index = 0;
        $col_index = 0;

        foreach ($fieldDef as $key => $def) {
            if (GridLayoutMetaDataParser::validField($def) && array_search($key, $lineItemFields)) {
                $targetModuleLang = return_module_language('en_us', $moduleObj->module_dir);
                $lableValue = $targetModuleLang[$def['vname']];

                if (empty($lableValue)) {
                    $targetAppLang = return_application_language('en_us');
                    $lableValue = $targetAppLang[$def['vname']];
                }
                $options = (!empty($def['options'])) ? $app_list_strings[$def['options']] : array();
                $panelRows[$row_index][$col_index] = array(
                    'name' => $def['name'],
                    'label' => $def['vname'],
                    'type' => $def['type'],
                    'required' => $def['required'],
                    'options' => $options,
                    'label_value' => $lableValue
                );
                $row_index++;
            }
        }
        return $panelRows;
    }
// Change on 13-05-2016: Resolve issue for Role.
    function getUser_accessibleModules($session, $user_id) {
        global $app_list_strings, $db, $current_user;
        $error = new SoapError();
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $module_name, 'read', 'no_access', $error)) {
            $GLOBALS['log']->info('End: SugarWebServiceImpl->get_modified_relationships');
            return;
        }
        $select = 'SELECT setting FROM user_accessible_module';
        $result = $db->query($select);
        $available_module = '';
        while ($row = $db->fetchByAssoc($result)) {
            $available_module = explode(',', $row['setting']);
        }
        $userAccessibleModulesList = query_module_access_list($current_user);
        $actions_array_list = array('edit', 'delete', 'list', 'view', 'export', 'import');
        foreach ($available_module as $u_setting_module) {
            $actions_array = array();
            if (!in_array($u_setting_module, $userAccessibleModulesList)) {
                foreach ($actions_array_list as $view) {
                    $actions_array[] = array('action' => $view, 'access' => 'false');
                }
            } else {
                foreach ($actions_array_list as $view) {
                    $actions_array[] = array('action' => $view, 'access' => 'true');
                }
            }
            $access_right_modules[] = array(
                'module_key' => $u_setting_module,
                'module_label' => $app_list_strings['moduleList'][$u_setting_module],
                'favorite_enabled' => '',
                'acls' => $actions_array
            );
        }
        $accessible_modules['modules'] = $access_right_modules;
        return $accessible_modules;
    }
// End
    function getUserTimezone($session, $module_name, $user_id) {
        $error = new SoapError();
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $module_name, 'read', 'no_access', $error)) {
            $GLOBALS['log']->info('End: SugarWebServiceImpl->get_modified_relationships');
            return;
        } // if

        $user = new User();
        $user->retrieve($user_id);
        $user->loadPreferences();
        $timezone = $user->getPreference('timezone');
        return $timezone;
    }

    function getDynamicOptions($session, $module_name) {
        global $app_list_strings, $beanList;
        self::$helperObject = new SugarWebServiceUtilv4_1_custom();
        $error = new SoapError();
        if (!self::$helperObject->checkSessionAndModuleAccess($session, 'invalid_session', $module_name, 'read', 'no_access', $error)) {
            $GLOBALS['log']->info('End: checkEmailAndSendUsername->getCasesRelatedToContact');
            return;
        }
        $module_obj = new $beanList[$module_name]();
        // getting requested module's field definations
        $field_defination = $module_obj->getFieldDefinitions();
        $dynamic_enum_fields = array();
        // selecting Dynamic dropdown fields
        foreach ($field_defination as $field => $defination) {
            if ($defination['type'] == 'dynamicenum') {
                $dynamic_enum_fields[$field] = $defination;
            }
        }

        $parent_fields = array();

        foreach ($dynamic_enum_fields as $dynamic_field => $dynamic_field_defs) {
            $dynamic_field_options = $app_list_strings[$dynamic_field_defs['options']];
            $parent_options_val = $app_list_strings[$field_defination[$dynamic_field_defs['parentenum']]['options']];
            $parent_values = array();
            $parent_values[$dynamic_field_defs['parentenum']] = $parent_options_val;

            foreach ($parent_values as $parentFieldName => $parentOptionValues) {
                foreach ($parentOptionValues as $parent_option_key => $parent_value) {
                    $arr_result = array();
                    foreach ($dynamic_field_options as $dkey => $dvalue) {
                        if (!empty($parent_option_key) && (strpos($dkey, $parent_option_key . '_') === 0)) {
                            $arr_result[$dkey] = array('name' => $dkey, 'value' => $dvalue);
                        }
                    }
                    $parent_fields[$parentFieldName][$parent_option_key][$dynamic_field] = $arr_result;
                }
            }
        }
        return $parent_fields;
    }

    function user_generate_password($user_info) {

        $mod_strings = return_module_language('', 'Users');
        $res = $GLOBALS['sugar_config']['passwordsetting'];
        $regexmail = "/^\w+(['\.\-\+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+\$/";

        $username = (!empty($user_info['user_name'])) ? trim($user_info['user_name']) : '';
        $useremail = (!empty($user_info['user_email'])) ? trim($user_info['user_email']) : '';

        $usr = new user();
        // check username and user email address are not empty
        if (!empty($username) && !empty($useremail)) {
            // retrieve user by username.
            $usr_id = $usr->retrieve_user_id($username);
            if (!empty($usr_id)) {
                $usr->retrieve($usr_id);
                // check user has provided it's primary emailaddress.
                if (!$usr->isPrimaryEmail($useremail)) {
                    return array('success' => false, 'error_msg' => $mod_strings['ERR_PASSWORD_USERNAME_MISSMATCH']);
                }
            } else {
                return array('success' => false, 'error_msg' => $mod_strings['ERR_PASSWORD_USERNAME_MISSMATCH']);
            }
        } else {
            return array('success' => false, 'error_msg' => $mod_strings['LBL_PROVIDE_USERNAME_AND_EMAIL']);
        }

        ///////  Check User's Primary emailaddress to send password.
        if (!preg_match($regexmail, $usr->emailAddress->getPrimaryAddress($usr))) {
            return array('success' => false, 'error_msg' => $mod_strings['ERR_EMAIL_INCORRECT']);
        }

        // if i need to generate a password (not a link)
        $password = User::generatePassword();

        $emailTemp_id = $res['generatepasswordtmpl'];

        $additionalData = array(
            'password' => $password
        );

        $result = $usr->sendEmailForPassword($emailTemp_id, $additionalData);
        $usr->setNewPassword($additionalData['password']);
        if ($result['status'] == true) {
            return array('success' => true, "massage" => $mod_strings["LBL_NEW_USER_PASSWORD_1"] . " " . $mod_strings["LBL_NEW_USER_PASSWORD_2"]);
        } else {
            return array('success' => false, 'error_msg' => $mod_strings['LBL_EMAIL_NOT_SENT']);
        }
    }

    function user_reset_password($session, $user_info) {

        $mod_strings = return_module_language('', 'Users');
        $current_password = (!empty($user_info['user_old_password'])) ? trim($user_info['user_old_password']) : "";
        $new_password = (!empty($user_info['user_new_password'])) ? trim($user_info['user_new_password']) : "";
        // retrieve user.
        if (!empty($user_info['id'])) {
            $user = new User();
            $user->retrieve($user_info['id']);

            if (!empty($current_password) && !empty($new_password)) {
                if ($user->change_password($current_password, $new_password, '0')) {
                    return array('success' => true, 'massage' => $mod_strings['LBL_NEW_USER_PASSWORD_1']);
                } else {
                    return array('success' => false, 'error_msg' => $user->error_string);
                }
            } else {
                return array('success' => false, 'error_msg' => $mod_strings['ERR_ENTER_NEW_PASSWORD']);
            }
        }
    }
    
    /* Modify get_entry and get_entry_list calls on 17-05-2016.
     * Resolve issue:- To get custom relationship data.
     */    
    function get_entry_list($session, $module_name, $query, $order_by, $offset, $select_fields, $link_name_to_fields_array, $max_results, $deleted, $favorites = false) {
        $listData = parent::get_entry_list($session, $module_name, $query, $order_by, $offset, $select_fields, $link_name_to_fields_array, $max_results, $deleted, $favorites = false);
        global $dictionary, $beanList;
        $moduleObj = $beanList[$module_name];
        if (!empty($listData['relationship_list'])) {
            foreach ($listData['relationship_list'] as $mainIdx => $dataDetails) {
                foreach ($dataDetails as $indx => $details) {
                    if (!empty($details)) {
                        foreach ($details as $linkData) {
                            $relationship_name = $linkData['name'];
                            foreach ($linkData['records'] as $detailData) {
                                $key_id = $dictionary[$moduleObj]['fields'][$relationship_name . '_name']['id_name'];
                                $key_name = $relationship_name . '_name';
                                foreach ($detailData as $d) {
                                    $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['name'] = $key_id;
                                    $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['value'] = $d['id']['value'];
                                    $listData['entry_list'][$mainIdx]['name_value_list'][$key_name]['value'] = $d['name']['value'];
                                }
                            }
                        }
                    } else {
                        $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['name'] = $key_id;
                        $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['value'] = '';
                        $listData['entry_list'][$mainIdx]['name_value_list'][$key_name]['value'] = '';
                    }
                }
            }
        }
        return $listData;
    }

    function get_entry($session, $module_name, $id, $select_fields, $link_name_to_fields_array, $track_view = FALSE) {
        $GLOBALS['log']->info('Begin: SugarWebServiceImpl->get_entry');
        $listData = parent::get_entry($session, $module_name, $id, $select_fields, $link_name_to_fields_array, $track_view);
        $GLOBALS['log']->info('end: SugarWebServiceImpl->get_entry');
        global $dictionary, $beanList;
        $moduleObj = $beanList[$module_name];
        if (!empty($listData['relationship_list'])) {
            foreach ($listData['relationship_list'] as $mainIdx => $dataDetails) {
                if (!empty($dataDetails)) {
                    foreach ($dataDetails as $linkData) {
                        $relationship_name = $linkData['name'];
                        foreach ($linkData['records'] as $detailData) {
                            $key_id = $dictionary[$moduleObj]['fields'][$relationship_name . '_name']['id_name'];
                            $key_name = $relationship_name . '_name';
                            $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['name'] = $key_id;
                            $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['value'] = $detailData['id']['value'];
                            $listData['entry_list'][$mainIdx]['name_value_list'][$key_name]['value'] = $detailData['name']['value'];
                        }
                    }
                } else {
                    $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['name'] = $key_id;
                    $listData['entry_list'][$mainIdx]['name_value_list'][$key_id]['value'] = '';
                    $listData['entry_list'][$mainIdx]['name_value_list'][$key_name]['value'] = '';
                }
            }
        }
        return $listData;
    }
    /*End*/

}

?>
