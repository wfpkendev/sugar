{literal}
    <script type="text/javascript">
        $("document").ready(function(){
            $("#error_span").remove();
        });
        function clearKey(){
            $("#licence_key").val("");
        }
        function redirectToindex(){
            location.href = "index.php?module=Administration&action=index";
        }
        function validateLicence(element){
            $("#error_span").remove();
            var key = $("#licence_key");
            if(key.val().trim() == ""){
                $("#clearkey").after("<span style=\'color:red;padding-left: 10px;\' id=\'error_span\'>Please enter valid Licence key.</span>")
                key.focus();
                return false;
            }else{
                $.ajax({
                    url:"index.php?module=Administration&action=LicenceHandler&method=validateLicence",
                    type:"POST",
                    data:{"k": key.val()},
                    beforeSend : function(){
                        $("#clearkey").after("<img style=\'color:red;padding-left: 10px;vertical-align: middle;\' id=\'survey_loader\' src= "+SUGAR.themes.loading_image+">");
                        $(element).attr("disabled","disabled");
                    },
                    complete : function(){
                        $("#survey_loader").remove();
                        $(element).removeAttr("disabled");
                    },
                    success:function(result){
                        licence_result = $.parseJSON(result);
                        $("#survey_loader").remove();
                        $("#enSelect option[value=0]").prop("selected", true);
                        if(licence_result.suc == 1){
                            $("#enableDiv").show();
                            $(".actionsContainerEnableDiv").show();
                            $(".actionsContainer").hide();
                            if(licence_result.allowed_user_count){
                                $("#userMagmtDiv").show();
                                $("#allowed_count").text(licence_result.allowed_user_count);
                                var remaining_count = licence_result.allowed_user_count - licence_result.remaining_count;
                                $("#remaining_count").text(remaining_count);
                            }

                            $("#clearkey").after("<span style=\'color:green;padding-left: 10px;\' id=\'error_span\'>License validated successfully.</span>");
                        }else{
                            $("#clearkey").after("<span style=\'color:red;padding-left: 10px;\' id=\'error_span\'>It seems some error occurs while validating license.</span>");
                            $("#enableDiv").hide();
                            $(".actionsContainerEnableDiv").hide();
                            $(".actionsContainer").show();
                            $("#userMagmtDiv").hide();
                        }

                        setTimeout(function () {
                            location.href = "index.php?module=Administration&action=licence";
                        }, 1000);




                    }
                });
            }
        }
        function enableUserBasePlugin(){
            var enabled = $("#enable").val();
            var lic_users_count = $("select[name=\'licensed_users[]\'] option").length;
            var avail_users = parseInt($("#allowed_users").val());
            if(avail_users >= lic_users_count){
                var lic_users = [];
                $("select[name=\'licensed_users[]\'] option").each(function() {
                    lic_users.push( $(this).attr("value"));
                });
                $.ajax({
                    url:"index.php?module=Administration&action=LicenceHandler&method=enableDisableModules",
                    data :{"enabled" : enabled,"lic_users":lic_users,"lic_users_count":lic_users_count},
                    type:"POST",
                    success:function(result){
                        if(enabled == "1"){
                            alert("Module enabled successfully.");
                        }else{
                            alert("Module disabled successfully.");
                        }
                        location.href = "index.php?module=Administration&action=index";
                    }
                });
            }else{
                $("#outfitters_license_increase").html("You have reached maximum allowed user limit.Please contact support@biztechconsultancy.com to exceed allowed user limit.");
            }
        }
        function enablePlugin(){
            var enabled = $("#enable").val();
            $.ajax({
                url:"index.php?module=Administration&action=LicenceHandler&method=enableDisableModules",
                data :{"enabled" : enabled},
                type:"POST",
                success:function(result){
                    if(enabled == "1"){
                        alert("Module enabled successfully.");
                    }else{
                        alert("Module disabled successfully.");
                    }
                    location.href = "index.php?module=Administration&action=index";
                }
            });
        }


    </script>
    <style type='text/css'>
        #unlicensed_users_td select {
            width: 170px !important;
        }
        #licensed_users_td select {
            width: 170px !important;
        }
        #outfitters_license_increase {
            color: red;
            font-weight: bold;
            font-size: 1.1em;
        }
    </style>
{/literal}
{assign var="display_enable" value="display:none;"}
{assign var="display_validate" value="display:none;"}
{if $LastValidation eq 1}
    {assign var="display_enable" value="display:block;"}
    {assign var="display_validate" value="display:none;"}
{/if}
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td colspan="100">
            <h2>
                <div class="moduleTitle">
                    <h2>{$plugin_name} Configuration </h2>

                    <div class="clear"></div>
                </div>
            </h2>
        </td>
    </tr>
    <tr>
        <td colspan="100">
            <div class="add_table" style="margin-bottom:5px">
                <table id="ConfigureSurvey" class="themeSettings edit view" style="margin-bottom:0px;" border="0"
                       cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <th align="left" colspan="4" scope="row"><h4>Validate Licence</h4></th>
                    </tr>
                    <tr>
                        <td scope="row" nowrap="nowrap" style="width: 10%;"><label for="name_basic"> Licence Key
                                : </label></td>
                        <td nowrap="nowrap" style="width: 15%;"><input name="licence_key" id="licence_key" size="30"
                                                                       maxlength="255" value="{$licenseKey}" title=""
                                                                       accesskey="9" type="text"></td>
                        <td nowrap="nowrap" style="width: 20%;"><input title="Validate" id="Validate"
                                                                       class="button primary"
                                                                       onclick="validateLicence(this);" name="validate"
                                                                       value="Validate" type="button">&nbsp;<input
                                    title="Clear" id="clearkey" class="button primary" onclick="clearKey();"
                                    name="clear" value="Clear" type="button"></td>
                        <td nowrap="nowrap" style="width: 55%;">&nbsp;</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <table class="actionsContainer" style="{$display_back}" border="0" cellpadding="1"
                   cellspacing="1">
                <tbody>
                <tr>
                    <td>
                        <input title="Back" accesskey="l" class="button" onclick="redirectToindex();" name="button"
                               value="Back" type="button">
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<div class="add_table" id="enableDiv" style="margin-bottom:5px;{$display_enable}">
    <table id="ConfigureSurvey" class="themeSettings edit view" style="margin-bottom:0px;" border="0" cellpadding="0"
           cellspacing="0">
        <tbody>
        <tr>
            <th align="left" colspan="4" scope="row"><h4>Enable/Disable Module</h4></th>
        </tr>
        <tr>
            <td scope="row" nowrap="nowrap" style="width: 10%;"><label for="name_basic"> Enable/Disable </label></td>
            <td nowrap="nowrap" style="width: 15%;">
                <select name="enable" id="enable">
                    {if $ModuleEnabled eq 1}
                        <option value="1" selected="">Enable</option>
                        <option value="0">Disable</option>
                    {else}
                        <option value="1">Enable</option>
                        <option value="0" selected="">Disable</option>
                    {/if}
                </select>
            </td>
            <td nowrap="nowrap" style="width: 20%;">&nbsp;</td>
            <td nowrap="nowrap" style="width: 55%;">&nbsp;</td>
        </tr>
        </tbody>
    </table>
    {if $is_user_base}
</div>
<div class="add_table" id="userMagmtDiv" style="margin-bottom:5px;{$display_enable}">
    <input type="hidden" name="allowed_users" id="allowed_users" value="{$AllowedUserCount}"/>
    <table id="ConfigureSurvey" class="themeSettings edit view" style="margin-bottom:0px;" border="0" cellpadding="0"
           cellspacing="0">
        <tbody>
        <tr>
            <th align="left" colspan="4" scope="row"><h4>Manage Users</h4></th>
        </tr>
        <tr>
            <td scope="row" nowrap="nowrap">
                <table>
                    <tr>
                        <td id="outfitters_license_increase"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td scope="row" nowrap="nowrap" style="padding:0 !important;">
                <table width="400px">
                    <tr>
                        <td scope="row" nowrap="nowrap" style="width: 200px;"><label for="name_basic">Total Allowed
                                Licenced User : </label></td>
                        <td scope="row" nowrap="nowrap"><span id="allowed_count">{$AllowedUserCount}</span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td scope="row" nowrap="nowrap" style="padding:0 !important;">
                <table width="400px">
                    <tr>
                        <td scope="row" nowrap="nowrap" style="width: 200px;"><label for="name_basic">Remaining Licenced
                                User : </label></td>
                        <td scope="row" nowrap="nowrap"><span id="remaining_count">{$remaining_lic_count}</span></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            {/if}
        <tr>
            <td>
                {$user_management}
            </td>
        </tr>
        </tbody>
    </table>
</div>
</div>
<table class="actionsContainerEnableDiv" style="{$display_enable}" border="0" cellpadding="1" cellspacing="1">
    <tbody>
    <tr>
        <td>
            <input title="Save" accesskey="a" class="button primary" {$OnClickEvent} name="button"
                   value="Save" type="submit">
            <input title="Cancel" accesskey="l" class="button" onclick="redirectToindex();" name="button" value="Cancel"
                   type="button">
        </td>
    </tr>
    </tbody>
</table>
</td></tr></tbody></table>