<?php
// created: 2016-02-09 14:06:07
$dictionary["field_helpdesk_cases"] = array (
  'true_relationship_type' => 'one-to-many',
  'relationships' => 
  array (
    'field_helpdesk_cases' => 
    array (
      'lhs_module' => 'field_Helpdesk',
      'lhs_table' => 'field_helpdesk',
      'lhs_key' => 'id',
      'rhs_module' => 'Cases',
      'rhs_table' => 'cases',
      'rhs_key' => 'id',
      'relationship_type' => 'many-to-many',
      'join_table' => 'field_helpdesk_cases_c',
      'join_key_lhs' => 'field_helpdesk_casesfield_helpdesk_ida',
      'join_key_rhs' => 'field_helpdesk_casescases_idb',
    ),
  ),
  'table' => 'field_helpdesk_cases_c',
  'fields' => 
  array (
    0 => 
    array (
      'name' => 'id',
      'type' => 'varchar',
      'len' => 36,
    ),
    1 => 
    array (
      'name' => 'date_modified',
      'type' => 'datetime',
    ),
    2 => 
    array (
      'name' => 'deleted',
      'type' => 'bool',
      'len' => '1',
      'default' => '0',
      'required' => true,
    ),
    3 => 
    array (
      'name' => 'field_helpdesk_casesfield_helpdesk_ida',
      'type' => 'varchar',
      'len' => 36,
    ),
    4 => 
    array (
      'name' => 'field_helpdesk_casescases_idb',
      'type' => 'varchar',
      'len' => 36,
    ),
  ),
  'indices' => 
  array (
    0 => 
    array (
      'name' => 'field_helpdesk_casesspk',
      'type' => 'primary',
      'fields' => 
      array (
        0 => 'id',
      ),
    ),
    1 => 
    array (
      'name' => 'field_helpdesk_cases_ida1',
      'type' => 'index',
      'fields' => 
      array (
        0 => 'field_helpdesk_casesfield_helpdesk_ida',
      ),
    ),
    2 => 
    array (
      'name' => 'field_helpdesk_cases_alt',
      'type' => 'alternate_key',
      'fields' => 
      array (
        0 => 'field_helpdesk_casescases_idb',
      ),
    ),
  ),
);