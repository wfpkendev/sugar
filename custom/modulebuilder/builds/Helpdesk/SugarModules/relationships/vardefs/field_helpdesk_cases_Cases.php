<?php
// created: 2016-02-09 14:06:07
$dictionary["Case"]["fields"]["field_helpdesk_cases"] = array (
  'name' => 'field_helpdesk_cases',
  'type' => 'link',
  'relationship' => 'field_helpdesk_cases',
  'source' => 'non-db',
  'module' => 'field_Helpdesk',
  'bean_name' => false,
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_FIELD_HELPDESK_TITLE',
  'id_name' => 'field_helpdesk_casesfield_helpdesk_ida',
);
$dictionary["Case"]["fields"]["field_helpdesk_cases_name"] = array (
  'name' => 'field_helpdesk_cases_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_FIELD_HELPDESK_TITLE',
  'save' => true,
  'id_name' => 'field_helpdesk_casesfield_helpdesk_ida',
  'link' => 'field_helpdesk_cases',
  'table' => 'field_helpdesk',
  'module' => 'field_Helpdesk',
  'rname' => 'name',
);
$dictionary["Case"]["fields"]["field_helpdesk_casesfield_helpdesk_ida"] = array (
  'name' => 'field_helpdesk_casesfield_helpdesk_ida',
  'type' => 'link',
  'relationship' => 'field_helpdesk_cases',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_FIELD_HELPDESK_CASES_FROM_CASES_TITLE',
);
