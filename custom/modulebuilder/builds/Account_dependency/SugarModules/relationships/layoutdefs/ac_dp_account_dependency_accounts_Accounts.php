<?php
 // created: 2014-04-24 16:43:18
$layout_defs["Accounts"]["subpanel_setup"]['ac_dp_account_dependency_accounts'] = array (
  'order' => 100,
  'module' => 'ac_dp_Account_dependency',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_AC_DP_ACCOUNT_DEPENDENCY_ACCOUNTS_FROM_AC_DP_ACCOUNT_DEPENDENCY_TITLE',
  'get_subpanel_data' => 'ac_dp_account_dependency_accounts',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
