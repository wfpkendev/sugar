<?php
// created: 2014-04-24 16:43:18
$dictionary["Account"]["fields"]["ac_dp_account_dependency_accounts"] = array (
  'name' => 'ac_dp_account_dependency_accounts',
  'type' => 'link',
  'relationship' => 'ac_dp_account_dependency_accounts',
  'source' => 'non-db',
  'side' => 'right',
  'vname' => 'LBL_AC_DP_ACCOUNT_DEPENDENCY_ACCOUNTS_FROM_AC_DP_ACCOUNT_DEPENDENCY_TITLE',
);
