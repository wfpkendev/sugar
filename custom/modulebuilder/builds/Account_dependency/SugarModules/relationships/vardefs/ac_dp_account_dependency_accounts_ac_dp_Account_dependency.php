<?php
// created: 2014-04-24 16:43:18
$dictionary["ac_dp_Account_dependency"]["fields"]["ac_dp_account_dependency_accounts"] = array (
  'name' => 'ac_dp_account_dependency_accounts',
  'type' => 'link',
  'relationship' => 'ac_dp_account_dependency_accounts',
  'source' => 'non-db',
  'vname' => 'LBL_AC_DP_ACCOUNT_DEPENDENCY_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'id_name' => 'ac_dp_account_dependency_accountsaccounts_ida',
);
$dictionary["ac_dp_Account_dependency"]["fields"]["ac_dp_account_dependency_accounts_name"] = array (
  'name' => 'ac_dp_account_dependency_accounts_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_AC_DP_ACCOUNT_DEPENDENCY_ACCOUNTS_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'ac_dp_account_dependency_accountsaccounts_ida',
  'link' => 'ac_dp_account_dependency_accounts',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["ac_dp_Account_dependency"]["fields"]["ac_dp_account_dependency_accountsaccounts_ida"] = array (
  'name' => 'ac_dp_account_dependency_accountsaccounts_ida',
  'type' => 'link',
  'relationship' => 'ac_dp_account_dependency_accounts',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_AC_DP_ACCOUNT_DEPENDENCY_ACCOUNTS_FROM_AC_DP_ACCOUNT_DEPENDENCY_TITLE',
);
