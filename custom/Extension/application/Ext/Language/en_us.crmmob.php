<?php

include 'custom/PluginConfiguration/config.php';
$plugin_name = $PluginConfig['plugin_name'];
$app_strings['VALIDATE_ERROR'] = 'There seems some error while validating your license for ' . $plugin_name . ' module. Please try again later.';
$app_strings['VALIDATE_FAIL'] = 'Please contact your Administrator to validate License.';
$app_strings['USER_VALIDATE_FAIL'] = 'You are not authorized user for this Application.Please contact your Administrator to authorize you.';
