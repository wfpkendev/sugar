<?php
 // created: 2014-04-24 16:27:21
$dictionary['Account']['fields']['account_type']['default']='Beneficiary';
$dictionary['Account']['fields']['account_type']['len']=100;
$dictionary['Account']['fields']['account_type']['required']=true;
$dictionary['Account']['fields']['account_type']['comments']='The account holder is of this type';
$dictionary['Account']['fields']['account_type']['merge_filter']='disabled';

 ?>