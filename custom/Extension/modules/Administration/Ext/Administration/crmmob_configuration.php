<?php
if (!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

$links = array();
global $mod_strings;
$links['Administration']['license_configuration_link'] =
    array(
        'License',
        'LBL_LICENSE_CONFIGURATION',
        'LBL_LICENSE_CONFIGURATION_DESC',
        'index.php?module=Administration&action=licence'
    );
$links['Administration']['configure_mob_link'] =
    array(
        'ConfigureTabs',
        'LBL_CRMMOB_CONFIGURE_MODULE_LINK',
        'LBL_CRMMOB_CONFIGURE_MODULE_LINK_DESC',
        './index.php?module=Administration&action=setcrmmoblayout',
    );
$links['Administration']['useraccessiblemodules']=
    array(
        'Administration',
        'LBL_ROLE_PER_USER_TITLE',
        'LBL_ROLE_PER_USER_DESC',
        './index.php?module=Administration&action=useraccessiblemodules'
    );

$admin_group_header[] =
    array(
        $mod_strings['LBL_CRMMOB_CONF_TITLE'],
        '',
        false,
        $links,
        $mod_strings['LBL_CRMMOB_CONF_DESC']
    );

