<?php

if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');
include 'custom/PluginConfiguration/config.php';
$plugin_name = $PluginConfig['plugin_name'];
$mod_strings['LBL_ADMIN_MODULE_NAME'] = 'Administration';
$mod_strings['LBL_CRMMOB_CONFIGURE_MODULE_LINK'] = 'Configure Mobile layouts';
$mod_strings['LBL_CRMMOB_CONFIGURE_MODULE_LINK_DESC'] = 'Set Mobile layout for each accessible Modules.';
$mod_strings['LBL_MODULE_NAME'] = 'Administration';
$mod_strings['LBL_CRMMOB_CONF_TITLE'] = 'Mobile Layout Configuration';
$mod_strings['LBL_CRMMOB_CONF_DESC'] = 'Mobile App Layout Configuration ';
$mod_strings['LBL_CRMMOB_CONF_LINK'] = 'Configuration';
$mod_strings['LBL_ROLE_PER_USER_TITLE'] = 'User Accessible Modules Setting';
$mod_strings['LBL_ROLE_PER_USER_DESC'] = 'Select modules which are accessible by all users of ' . $plugin_name;
$mod_strings['LBL_LICENSE_CONFIGURATION'] = $plugin_name . ' Licence Configuration';
$mod_strings['LBL_LICENSE_CONFIGURATION_DESC'] = 'Manage and configure the licence for ' . $plugin_name;

