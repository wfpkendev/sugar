<?php
// created: 2014-04-24 16:00:51
$dictionary["Project"]["fields"]["accounts_project_2"] = array (
  'name' => 'accounts_project_2',
  'type' => 'link',
  'relationship' => 'accounts_project_2',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_PROJECT_2_FROM_ACCOUNTS_TITLE',
  'id_name' => 'accounts_project_2accounts_ida',
);
$dictionary["Project"]["fields"]["accounts_project_2_name"] = array (
  'name' => 'accounts_project_2_name',
  'type' => 'relate',
  'source' => 'non-db',
  'vname' => 'LBL_ACCOUNTS_PROJECT_2_FROM_ACCOUNTS_TITLE',
  'save' => true,
  'id_name' => 'accounts_project_2accounts_ida',
  'link' => 'accounts_project_2',
  'table' => 'accounts',
  'module' => 'Accounts',
  'rname' => 'name',
);
$dictionary["Project"]["fields"]["accounts_project_2accounts_ida"] = array (
  'name' => 'accounts_project_2accounts_ida',
  'type' => 'link',
  'relationship' => 'accounts_project_2',
  'source' => 'non-db',
  'reportable' => false,
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_PROJECT_2_FROM_PROJECT_TITLE',
);
