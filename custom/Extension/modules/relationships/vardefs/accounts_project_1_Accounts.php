<?php
// created: 2014-04-24 16:00:34
$dictionary["Account"]["fields"]["accounts_project_1"] = array (
  'name' => 'accounts_project_1',
  'type' => 'link',
  'relationship' => 'accounts_project_1',
  'source' => 'non-db',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_PROJECT_1_FROM_PROJECT_TITLE',
);
