<?php
// created: 2014-04-24 16:00:51
$dictionary["Account"]["fields"]["accounts_project_2"] = array (
  'name' => 'accounts_project_2',
  'type' => 'link',
  'relationship' => 'accounts_project_2',
  'source' => 'non-db',
  'side' => 'right',
  'vname' => 'LBL_ACCOUNTS_PROJECT_2_FROM_PROJECT_TITLE',
);
