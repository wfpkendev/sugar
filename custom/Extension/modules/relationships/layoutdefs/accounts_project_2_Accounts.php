<?php
 // created: 2014-04-24 16:00:51
$layout_defs["Accounts"]["subpanel_setup"]['accounts_project_2'] = array (
  'order' => 100,
  'module' => 'Project',
  'subpanel_name' => 'default',
  'sort_order' => 'asc',
  'sort_by' => 'id',
  'title_key' => 'LBL_ACCOUNTS_PROJECT_2_FROM_PROJECT_TITLE',
  'get_subpanel_data' => 'accounts_project_2',
  'top_buttons' => 
  array (
    0 => 
    array (
      'widget_class' => 'SubPanelTopButtonQuickCreate',
    ),
    1 => 
    array (
      'widget_class' => 'SubPanelTopSelectButton',
      'mode' => 'MultiSelect',
    ),
  ),
);
